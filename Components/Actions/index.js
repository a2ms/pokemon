export const retriveDataLocal = (data) => {
  return {
    type: "RETRIVE_LOCAL",
    payload: JSON.parse(window.localStorage.getItem("reduxState"))
  };
};

export const addData = (data) => {
  return {
    type: "ADD",
    payload: data,
  };
};

export const releaseData = (id) => {
  return {
    type: "RELEASE",
    payload: id,
  };
};