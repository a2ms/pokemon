import { combineReducers } from "redux";
import myPokemons from "./myPokemons";

const allReducers = combineReducers({
  myPokemons: myPokemons,
});

export default allReducers;