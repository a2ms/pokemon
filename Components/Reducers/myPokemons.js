const initialState = {
  myListPokemon: [],
};

export default function pokemonApi(state = initialState, action) {
  switch(action.type) {
      case "RETRIVE_LOCAL":
        return {
          ...state,
          myListPokemon: action.payload.myPokemons.myListPokemon
        }
    
      case "ADD":
        return{
          ...state,
          myListPokemon: [...state.myListPokemon, action.payload]
        }
      case "RELEASE":
        return {
          ...state,
          myListPokemon: state.myListPokemon.filter(myListPokemon => myListPokemon.id !== action.payload),
        }

    // case POKEMON_DELETED_OK:
    //   return {
    //     ...state,
    //     listPokemon: state.listPokemon.filter(item => item.id !== state.deleteProduct),
    //   }

    default:
      return state
  }
}