import Head from "next/head";
import { useEffect } from "react"
import allReducers from "../Components/Reducers/index";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import { retriveDataLocal } from "../Components/Actions";

const store = createStore(allReducers, applyMiddleware(thunk));

store.subscribe(() => {
  localStorage.setItem("reduxState", JSON.stringify(store.getState()));
});

function MyApp({ Component, pageProps }) {
  useEffect(() => {
    (async () => {
      await store.dispatch(retriveDataLocal())
    })()
  }, [])
  return (
    <>
      <Head>
        <title>Pokemons</title>
        <link rel="shortcut icon" href="/pokeball.svg" />
      </Head>
      <Provider store={store}>
        <Component {...pageProps} />
      </Provider>
    </>
  );
}

export default MyApp;
