/** @jsxRuntime classic */
/** @jsx jsx */
import { jsx } from "@emotion/react";
import * as GlobalStyles from "../../styles/global.style";
import Link from "next/link";

import { gql } from "@apollo/client";
import client from "../../apollo-client";
import { withRouter } from "next/router";
import { Component } from "react";
import { useDispatch } from "react-redux";
import { connect } from "react-redux";
import { addData } from "../../Components/Actions";

class detailPage extends Component {
  static async getInitialProps(router) {
    const gqlVariables = {
      name: router.query.name,
    };

    const { data } = await client.query({
      variables: gqlVariables,
      query: gql`
        query pokemon($name: String!) {
          pokemon(name: $name) {
            id
            name
            sprites {
              front_default
            }
            moves {
              move {
                name
              }
            }
            types {
              type {
                name
              }
            }
          }
        }
      `,
    });
    return {
      pokemon: data.pokemon,
    };
  }


  constructor(props) {
    super(props);
    this.state = {
      catch: null,
      pokemon: this.props.pokemon,
      nickname: "",
      err_nickname: false,
      my_pokemon_list: this.props.myListPokemon || [],
    };
  }

  getRandom = () => {
    const random_boolean = Math.random() < 0.5;
    this.setState({
      catch: random_boolean,
    });
  };

  validateNickname = (e) => {
    if (this.state.my_pokemon_list.find((value) => {return value.nickname == e.target.value })) {
      this.setState({
        err_nickname: true,
      });
    } else {
      this.setState({
        err_nickname: false,
        nickname: e.target.value,
      });
    }
  };

  onSave = () => {
    if (this.state.nickname !== "" && !this.state.err_nickname) {
      const params = {
        id: Date.now(),
        name: this.state.pokemon.name,
        image: this.state.pokemon.sprites.front_default,
        type: this.state.pokemon.types,
        nickname: this.state.nickname,
      };
      const { dispatch } = this.props;
      dispatch(addData(params));
      this.props.router.push('/home/myPokemon')
    }
  };

  render() {
    return (
      <div>
        <div css={GlobalStyles.header}>
          <div css={GlobalStyles.headerContent}>
            <Link href="/home">
              <img src="/leftArrow.svg" />
            </Link>
            <div css={GlobalStyles.head__title}>Pokemon</div>
            <div css={GlobalStyles.headerContentButton}>
              <img src="/pokeball.svg" width={16} />
              <div>MyPokemon</div>
            </div>
          </div>
        </div>
        <div css={GlobalStyles.DetailPageContainer}>
          <div css={GlobalStyles.row}>
            <div css={GlobalStyles.col3}>
              <div css={GlobalStyles.Card}>
                <div css={GlobalStyles.Card__figure}>
                  <img
                    src={this.state.pokemon.sprites.front_default}
                    width={250}
                  />
                </div>
                <div css={GlobalStyles.Card__header}>
                  <div css={GlobalStyles.Card__title}>
                    {this.state.pokemon.name}
                  </div>
                </div>
                <div css={GlobalStyles.Card__chips}>
                  {this.state.pokemon.types.map((type) => (
                    <div key={type.type.name} css={GlobalStyles.Chips}>
                      {type.type.name}
                    </div>
                  ))}
                </div>
                <div css={GlobalStyles.Action__Card}>
                  {this.state.catch === true ? (
                    <div css={GlobalStyles.catched}>
                      <img src="/catch.svg" width={50} />
                      <div>
                        congrats you caught pokemon {this.state.pokemon.name}
                      </div>
                      <div>
                        <input
                          css={GlobalStyles.input_nickname}
                          type="text"
                          placeholder="Input Nickname"
                          onInput={this.validateNickname}
                        />
                        {this.state.err_nickname === true ? (
                          <div style={{ color: "red" }}>
                            Nickname already have been taken
                          </div>
                        ) : (
                          <div style={{ color: "green" }} />
                        )}
                      </div>
                      <button
                        onClick={this.onSave}
                        css={GlobalStyles.btn__list}
                      >
                        Save
                      </button>
                    </div>
                  ) : this.state.catch === false ? (
                    <div css={GlobalStyles.uncatched}>
                      <img src="/pikachu.svg" width={50} />
                      <div>
                        ooh no you failed to catch pokemon{" "}
                        {this.state.pokemon.name}
                      </div>
                      <Link href="/home">
                        <button css={GlobalStyles.btn_back__home}>
                          Back to Home Page
                        </button>
                      </Link>
                    </div>
                  ) : (
                    <button
                      onClick={this.getRandom}
                      css={GlobalStyles.Button__Catch}
                    >
                      Catch
                    </button>
                  )}
                </div>
              </div>
            </div>
            <div css={GlobalStyles.col8}>
              <div css={GlobalStyles.Card__detail}>
                <div>Detail Pokemon Moves</div>
                <ul css={GlobalStyles.List__ul}>
                  {this.state.pokemon.moves.map((move, index) => (
                    <li css={GlobalStyles.List__li} key={index}>
                      {move.move.name}
                    </li>
                  ))}
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    myListPokemon: state.myPokemons.myListPokemon,
  };
};

export default connect(mapStateToProps)(withRouter(detailPage));
