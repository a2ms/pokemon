/** @jsxRuntime classic */
/** @jsx jsx */
import { jsx } from "@emotion/react";
import * as GlobalStyles from "../../styles/global.style";
import Link from "next/link";

import { gql } from "@apollo/client";
import client from "../../apollo-client";

import { connect } from "react-redux";
import { Component } from "react";

import { retriveDataLocal } from "../../Components/Actions";

class Home extends Component {
  static async getInitialProps() {
    const gqlVariables = {
      limit: 20,
      offset: 1,
    };

    const { data } = await client.query({
      variables: gqlVariables,
      query: gql`
        query pokemons($limit: Int, $offset: Int) {
          pokemons(limit: $limit, offset: $offset) {
            count
            next
            previous
            status
            message
            results {
              id
              name
              image
            }
          }
        }
      `,
    });
    return {
      pokemons: data.pokemons,
    };
  }
  constructor(props) {
    super(props);
    this.state = {
      pokemons: this.props.pokemons,
    };
  }

  getOwned = (name) => {
    const count = this.props.myListPokemon.filter((item) => item.name === name)
      .length;
    return count;
  };

  getLocalStorage = () => {
    const { dispatch } = this.props;
    const persistedState = localStorage.getItem('reduxState') 
    const temp = JSON.parse(persistedState)
    if (temp !== undefined && temp !== null) {
    dispatch(retriveDataLocal());
    }
  };

  componentDidMount() {
    if (this.props.myListPokemon.length === 0) {
      this.getLocalStorage();
    }
  }

  render() {
    return (
      <div>
        <div css={GlobalStyles.header}>
          <div css={GlobalStyles.headerContent}>
            <div css={GlobalStyles.head__title}>Pokemon</div>
            <Link href="/home/myPokemon">
              <div css={GlobalStyles.headerContentButton}>
                <img src="/pokeball.svg" width={16} />
                <div>MyPokemon</div>
              </div>
            </Link>
          </div>
        </div>
        <div css={GlobalStyles.HomePageContainer}>
          <div css={GlobalStyles.row}>
            {this.state.pokemons.results.map((pokemon) => (
              <div css={GlobalStyles.col6} key={pokemon.id}>
                <Link
                  href={{
                    pathname: "/home/detailPage",
                    query: { name: pokemon.name },
                  }}
                >
                  <div css={GlobalStyles.Card__List}>
                    <div css={GlobalStyles.Card__figure}>
                      <img src={pokemon.image} width={250} />
                    </div>
                    <div css={GlobalStyles.Card__header}>
                      <div css={GlobalStyles.Card__title}>{pokemon.name}</div>
                    </div>
                    <div css={GlobalStyles.Chips__Owned}>
                      <div css={GlobalStyles.Chips}>
                        Owned: {this.getOwned(pokemon.name)}
                      </div>
                    </div>
                  </div>
                </Link>
              </div>
            ))}
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    myListPokemon: state.myPokemons.myListPokemon,
  };
};

export default connect(mapStateToProps)(Home);
