/** @jsxRuntime classic */
/** @jsx jsx */
import { jsx } from "@emotion/react";
import * as GlobalStyles from "../../styles/global.style";
import Link from "next/link";

import { connect } from "react-redux";
import { Component } from "react";
import { withRouter } from "next/router";


import { retriveDataLocal, releaseData } from "../../Components/Actions";

class MyPokemon extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pokemons: [],
    };
  }

  async getData() {
    await this.props.onGetData();
    this.setState({
      pokemons: this.props.myListPokemon,
    });
  }

  onRelease = (id) => {
    console.log(id);
    this.props.onReleaseData(id)
    this.props.router.reload('/home/myPokemon')
  };

  componentDidMount() {
    this.getData();
  }

  render() {
    return (
      <div>
        <div css={GlobalStyles.header}>
          <div css={GlobalStyles.headerContent}>
            <Link href="/home">
              <img src="/leftArrow.svg" />
            </Link>
            <div css={GlobalStyles.head__title}>Pokemon</div>
            <div css={GlobalStyles.headerContentButton}>
              <img src="/pokeball.svg" width={16} />
              <div>MyPokemon</div>
            </div>
          </div>
        </div>
        <div css={GlobalStyles.HomePageContainer}>
          {this.state.pokemons.length === 0 ? (
            <div>No Data Available</div>
          ) : (
            <div css={GlobalStyles.row}>
              {this.state.pokemons.map((pokemon) => (
                <div css={GlobalStyles.col6} key={pokemon.id}>
                  <div css={GlobalStyles.Card__List}>
                    <div css={GlobalStyles.Card__figure}>
                      <img src={pokemon.image} width={250} />
                    </div>
                    <div css={GlobalStyles.Card__header}>
                      <div css={GlobalStyles.Card__title}>{pokemon.name}</div>
                    </div>
                    <div css={GlobalStyles.Chips__Owned}>
                      <div css={GlobalStyles.Chips_nickname}>
                        Nickname: {pokemon.nickname}
                      </div>
                    </div>
                    <div css={GlobalStyles.Action__Card}>
                      <button
                        onClick={() => {
                          this.onRelease(pokemon.id);
                        }}
                        css={GlobalStyles.Button__Release}
                      >
                        remove/release
                      </button>
                    </div>
                  </div>
                </div>
              ))}
            </div>
          )}
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    myListPokemon: state.myPokemons.myListPokemon,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onGetData: () => dispatch(retriveDataLocal()),
    onReleaseData: (id) => dispatch(releaseData(id)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(MyPokemon));
