/** @jsxRuntime classic */
/** @jsx jsx */
import { jsx } from '@emotion/react'
import React, { useEffect } from "react";
import { useRouter } from "next/router";

function app() {
  const router = useRouter();
  useEffect(() => {
    setTimeout(() => {
      router.push("/home");
    }, 3000);
  });

  return (
      <div>
        <div className="flex flex-col min-h-full flex-grow items-center justify-center">
          <div>POKEMON PET SHOP</div>
        </div>
      </div>
  );
}

export default app;
