import { css } from "@emotion/react";

export const head__title = css`
  font-size: 20px;
  color: #4caf50;
  font-weight: 600;
`;

export const row = css`
  display: flex;
  flex-wrap: wrap;
  flex: 1 1 auto;
`;

export const col1 = css`
  flex: 0 0 8.33%;
  max-width: 8.33%;
  padding: 12px;
  @media (min-width: 500px) {
    padding: 0px;
    min-width: 100%;
  }
`;
export const col2 = css`
  flex: 0 0 16.66%;
  max-width: 16.66%;
  padding: 12px;
  @media (max-width: 500px) {
    padding: 0px;
    min-width: 100%;
  }
`;
export const col3 = css`
  flex: 0 0 25%;
  max-width: 25%;
  padding: 12px;
  @media (max-width: 500px) {
    padding: 0px;
    min-width: 100%;
  }
`;
export const col4 = css`
  flex: 0 0 33.33%;
  max-width: 33.33%;
  padding: 12px;
  @media (max-width: 500px) {
    padding: 0px;
    min-width: 100%;
  }
`;
export const col5 = css`
  flex: 0 0 41.66%;
  max-width: 41.66%;
  padding: 12px;
  @media (max-width: 500px) {
    padding: 0px;
    min-width: 100%;
  }
`;
export const col6 = css`
  flex: 0 0 50%;
  max-width: 50%;
  @media (max-width: 500px) {
    padding: 0px;
    min-width: 100%;
  }
`;
export const col7 = css`
  flex: 0 0 58.33%;
  max-width: 58.33%;
  padding: 12px;
  @media (max-width: 500px) {
    padding: 0px;
    min-width: 100%;
  }
`;
export const col8 = css`
  flex: 0 0 66.66%;
  max-width: 66.66%;
  padding: 12px;
  padding: 12px;
  @media (max-width: 500px) {
    padding: 0px;
    min-width: 100%;
  }
`;
export const col9 = css`
  flex: 0 0 75%;
  max-width: 75%;
  padding: 12px;
  @media (max-width: 500px) {
    padding: 0px;
    min-width: 100%;
  }
`;
export const col10 = css`
  flex: 0 0 83.33%;
  max-width: 83.33%;
  padding: 12px;
  @media (max-width: 500px) {
    padding: 0px;
    min-width: 100%;
  }
`;
export const col11 = css`
  flex: 0 0 91.66%;
  max-width: 91.66%;
  padding: 12px;
  @media (max-width: 500px) {
    padding: 0px;
    min-width: 100%;
  }
`;
export const col12 = css`
  flex: 0 0 100%;
  max-width: 100%;
  padding: 12px;
  @media (max-width: 500px) {
    padding: 0px;
    min-width: 100%;
  }
`;

export const header = css`
  background: #ffffff;
  box-shadow: 0px 2px 8px rgba(51, 51, 51, 0.15);
  padding: 15px;
`;

export const headerContent = css`
  display: flex;
  align-items: center;
`;

export const headerContentButton = css`
  display: flex;
  align-items: center;
  text-decoration: underline;
  margin-left: auto;
  cursor: pointer;
`;

export const PageContainer = css`
  background: #eeeef1;
  max-width: 100%;
  padding: 0;
  margin: 0;
  font-family: -apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Oxygen,
    Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue, sans-serif;
`;

export const HomePageContainer = css`
  max-width: 100%;
  padding: 24px;
  margin-right: auto;
  margin-left: auto;
`;

export const DetailPageContainer = css`
  max-width: 100%;
  padding: 24px;
  margin-right: auto;
  margin-left: auto;
`;

export const Card = css`
  background: #ffffff;
  border-radius: 0.5rem;
  box-shadow: 0 0 0 0 rgba(0, 0, 0, 0.4);
`;

export const Card__List = css`
  background: #ffffff;
  border-radius: 0.5rem;
  box-shadow: 0 0 0 0 rgba(0, 0, 0, 0.4);
  margin: 12px;
`;

export const Card__detail = css`
  background: #ffffff;
  border-radius: 0.5rem;
  max-width: 100%;
  padding: 24px;
  @media (max-width: 500px) {
    margin-top: 24px;
    padding: 12px;
  }
`;

export const List__ul = css`
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  @media (max-width: 500px) {
    display: grid;
    grid-template-columns: repeat(2, 1fr);
  }
`;

export const List__li = css`
  font-size: 10px;
  @media (min-width: 756px) {
    font-size: 12px;
  }
`;

export const catched = css`
  background: #e8f8e8;
  border: 1px solid #1cb71c;
  box-sizing: border-box;
  font-size: 12px;
  border-radius: 8px;
  padding: 10px;
  color: #49c549;
  text-align: center;
  @media (min-width: 756px) {
    font-size: 16px;
  }
`;

export const uncatched = css`
  background: #fae9ea;
  border: 1px solid #cf212a;
  box-sizing: border-box;
  font-size: 12px;
  border-radius: 8px;
  padding: 10px;
  color: #cf212a;
  text-align: center;
  @media (min-width: 756px) {
    font-size: 16px;
  }
`;

export const Card__figure = css`
  display: flex;
  justify-content: center;
  align-items: center;
  position: relative;
  margin: 0;
  /* Visual */
  background-color: #eaeaea;
  background-image: linear-gradient(
    3deg,
    rgb(255, 255, 255) 14.8%,
    rgb(255, 245, 3) 15%,
    rgb(85, 202, 237) 75%
  );
  border-radius: 0.5rem 0.5rem 0 0;
`;

export const Card__header = css`
  position: relative;
  margin: 0;
  padding: 0 1rem;
`;

export const Card__title = css`
  font-size: 30px;
  font-weight: 400;
`;

export const Card__chips = css`
  margin: 1rem;
`;

export const Chips__Owned = css`
  padding: 1rem;
`;

export const Chips = css`
  align-items: center;
  display: inline-flex;
  justify-content: center;

  /* Background color */
  background-color: rgba(0, 0, 0, 0.1);

  /* Rounded border */
  border-radius: 9999px;
  margin-right: 0.5rem;
  /* Spacing */
  padding: 4px 8px;
`;

export const Chips_nickname = css`
  align-items: center;
  display: inline-flex;
  justify-content: center;

  background: #e8f8e8;
  border: 1px solid #1cb71c;

  /* Rounded border */
  border-radius: 9999px;
  margin-right: 0.5rem;
  /* Spacing */
  padding: 4px 8px;
`;

export const Button__Catch = css`
  background-color: #ffffff;
  color: black;
  border: 2px solid #4caf50;
  padding: 15px 32px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
  width: 100%;
  border-radius: 0.5rem;
  cursor: pointer;
  &:hover {
    background-color: #4caf50;
    color: white;
  }
`;


export const Button__Release = css`
  background-color: #ffffff;
  color: #cf212a;
  border: 2px solid #cf212a;
  padding: 15px 32px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
  width: 100%;
  border-radius: 0.5rem;
  cursor: pointer;
  &:hover {
    background-color: #cf212a;
    color: white;
  }
`;

export const Action__Card = css`
  padding: 1rem;
`;

export const btn__list = css`
  background-color: #4caf50;
  border: none;
  color: white;
  padding: 8px 20px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  margin: 8px 2px;
  cursor: pointer;
  border-radius: 0.5rem;
`;

export const btn_back__home = css`
  background-color: #cf212a;
  border: none;
  color: white;
  padding: 8px 20px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  margin: 4px 2px;
  cursor: pointer;
  border-radius: 0.5rem;
`;

export const input_nickname = css`
  border: 1px solid #ccc;
  padding: 7px 14px 9px;
  transition: 0.4s;
  margin: 10px 0px 10px 0px;
`;
